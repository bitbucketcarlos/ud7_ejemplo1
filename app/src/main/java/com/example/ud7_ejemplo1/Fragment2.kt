package com.example.ud7_ejemplo1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class Fragment2 : Fragment()  {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // "Inflamos" el Fragment
        val view = inflater.inflate(R.layout.layout_fragment2, container, false)

        return view
    }
}