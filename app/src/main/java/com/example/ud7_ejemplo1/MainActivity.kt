package com.example.ud7_ejemplo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.ud7_ejemplo1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Mostramos el icono de la ToolBar
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_nav_menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        drawerLayout = binding.drawerLayout

        binding.navview.setNavigationItemSelectedListener {
            var fragmentTransaction = false
            lateinit var fragment: Fragment

            when(it.itemId){
                R.id.menu_seccion_1, R.id.menu_opcion_1 -> {
                    fragment = Fragment1()
                    fragmentTransaction = true
                }
                R.id.menu_seccion_2, R.id.menu_opcion_2 -> {
                    fragment = Fragment2()
                    fragmentTransaction = true
                }
                R.id.menu_seccion_3 -> {
                    fragment = Fragment3()
                    fragmentTransaction = true
                }
            }

            if(fragmentTransaction) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit()

                // Mostramos que la opción se ha pulsado.
                it.isChecked = true

                // Mostramos el título de la sección en el Toolbar.
                title = it.title
            }

            // Cerramos el Drawer
            drawerLayout.closeDrawer(GravityCompat.START)

            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            // Si está abierto lo cerramos sino lo abrimos.
            if(drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START)
            else
                drawerLayout.openDrawer(GravityCompat.START)

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        // Si está abierto al pulsar "Atrás" lo cerramos.
        if(drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }
}